bin/ex5: src/ex5.o src/myStuff.o src/myStuff2.o
	-@${MKDIR} bin
	${CLINKER} -o $@ $^ ${PETSC_LIB}
	${DSYMUTIL} $@
	${RM} $^

clean::
	${RM} bin/ex5
	${RM} -r bin/ex5.dSYM

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules